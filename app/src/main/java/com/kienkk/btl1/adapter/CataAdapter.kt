package com.kienkk.btl1.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kienkk.btl1.R
import com.kienkk.btl1.model.ListPhoto

class CataAdapter(var listP: MutableList<ListPhoto>, var rclImage: RecyclerView, var rclCata: RecyclerView): RecyclerView.Adapter<CataAdapter.ViewHolder>() {

    var temp: Int = 0
    var selected = -1

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var tvCata = itemView.findViewById<TextView>(R.id.cata)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_cata,parent,false)
        return ViewHolder(view)
    }

    fun setSelect(position: Int){
        selected = position
        notifyDataSetChanged()
        rclCata.scrollToPosition(position)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (selected == position){
            holder.tvCata.setTextColor(Color.parseColor("#7e87f1"))
        }else{
            holder.tvCata.setTextColor(Color.parseColor("#b1b3d1"))
        }
        holder.tvCata.text = listP[position].name
        holder.itemView.setOnClickListener {
            temp = 0
            for (i in 0..listP.size){
                if (i == position){
                    if (i == 0){
                        rclImage.scrollToPosition(temp)
                    }else{
                        rclImage.scrollToPosition(temp - 1)
                    }
                    break
                }
                else{
                    temp = temp + listP[position].totalImage
                }

            }
            selected = position
            notifyDataSetChanged()
            rclCata.scrollToPosition(position)
        }
    }

    override fun getItemCount(): Int {
        return listP.size
    }
}