package com.kienkk.btl1.adapter

import android.app.DownloadManager.ACTION_DOWNLOAD_COMPLETE
import android.content.Context
import android.content.Intent
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kienkk.btl1.MainActivity
import com.kienkk.btl1.R
import com.kienkk.btl1.model.ListPhoto
import com.kienkk.btl1.presenter.HomePresenter
import com.squareup.picasso.Picasso
import java.io.File
import android.app.DownloadManager

import android.content.BroadcastReceiver

import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.registerReceiver
import androidx.core.net.toUri
import com.squareup.picasso.Target


class ImageAdapter(var context: Context,
                   var image: List<String>,
                   var listP: MutableList<ListPhoto>,
                   var rclImage: RecyclerView,
                   var adapter: CataAdapter,
                   var mainActivity: MainActivity,
                   var ivImage: ImageView,
                   var nameImage: TextView) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {

    var positionCata = mutableListOf<Int>()
    var homePresenter = HomePresenter(mainActivity,context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_image,parent,false)
        if(positionCata.size == 0){
            positionCata.add(0)
            for (i in listP){
                positionCata.add(positionCata.last() + i.totalImage)
            }
        }
        return ViewHolder(view,context,rclImage,positionCata, adapter,this,homePresenter,image,ivImage,nameImage)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.with(mainActivity).load(image[position]).into(holder.itemImageView)
        val filename = image.get(position).substringAfterLast('/')
        var sd: File = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        val dest = File(sd, filename)

        var stt = filename.substringAfterLast("_")
        stt = stt.substringBeforeLast(".png")
        var nameCata = filename.substringBefore("_frame")

        if (stt == "1"){
            holder.itemView.layoutParams.width = 180
        }else{
            holder.itemView.layoutParams.width = 130
        }

        if (dest.exists()){
            holder.isDown.visibility = View.INVISIBLE
        }else{
            holder.isDown.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return image.size
    }

    class ViewHolder(itemView: View,context: Context, rclImage: RecyclerView, positionCata: MutableList<Int>, adapter: CataAdapter,adaterImage:ImageAdapter, homePresenter: HomePresenter, image: List<String>, ivImage: ImageView, nameImage: TextView ) : RecyclerView.ViewHolder(itemView) {
        var itemImageView : ImageView
        var isDown: ImageView
        var cardView: CardView
        init {
            itemImageView = itemView.findViewById(R.id.itemImageView)
            isDown = itemView.findViewById(R.id.is_download)
            cardView = itemView.findViewById(R.id.card_view)
            rclImage.addOnScrollListener(object: RecyclerView.OnScrollListener(){
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    var position = adapterPosition
                    for (i in positionCata){
                        if (position == i + 5){
                            adapter.setSelect(positionCata.indexOf(i))
                        }
                    }
                    super.onScrolled(recyclerView, dx, dy)
                }
            })

            itemView.setOnClickListener {
                val filename = image.get(position).substringAfterLast('/')
                var sd: File = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
                val dest = File(sd, filename)
                if (dest.exists()){
                    var bitmap = BitmapFactory.decodeFile(dest.absolutePath)
                    ivImage.setImageBitmap(bitmap)
                    nameImage.text = dest.name
                }else{
                    homePresenter.downloadFile(image[position],ivImage,isDown,nameImage)
                }

            }
        }

    }
}