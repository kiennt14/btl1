package com.kienkk.btl1

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.*
import androidx.core.app.ActivityCompat
import com.kienkk.btl1.fragment.HomeFragment
import com.kienkk.btl1.model.APIResponse
import com.kienkk.btl1.presenter.HomeInterface
import com.kienkk.btl1.presenter.HomePresenter

class MainActivity : AppCompatActivity(), HomeInterface{

    var i = 0 //Count time back
    var homePresenter = HomePresenter(this, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE"
            ),111
        )

        homePresenter.getDataFromAPI()

    }

    override fun onBackPressed() {
        i++
        if (i>1){
            super.onBackPressed()
        }else{
            Toast.makeText(applicationContext,"Back cái nữa để thoát app",Toast.LENGTH_SHORT).show()
            var handler = Handler(Looper.myLooper()!!).postDelayed({
                i = 0
            },1000)
        }
    }

    override fun StartFragment(data: APIResponse, uriImageAll: MutableList<String>) {
        var homeFragment: HomeFragment
        homeFragment = HomeFragment(uriImageAll,data!!.listPhotoFrames, this)
        supportFragmentManager.beginTransaction().replace(R.id.fregment,homeFragment).commit()
    }

    override fun OnClick() {
        TODO("Not yet implemented")
    }

}

