package com.kienkk.btl1.api

import com.kienkk.btl1.model.APIResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface APIService {

    companion object {
        var BASE_URL = "https://mystoragetm.s3.ap-southeast-1.amazonaws.com/"
        fun create() : APIService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(APIService::class.java)
        }
    }

    @GET("{link}")
    fun getImage(@Path("link") folder: String): Call<ResponseBody>

    @get:GET("frames_birthday.json")
    val getData : Call<APIResponse>

}