package com.kienkk.btl1.presenter

import com.kienkk.btl1.model.APIResponse

interface HomeInterface {
    fun StartFragment(data : APIResponse, uriImageAll : MutableList<String>)
    fun OnClick()
}