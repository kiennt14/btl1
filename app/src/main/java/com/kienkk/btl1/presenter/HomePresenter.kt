package com.kienkk.btl1.presenter

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.kienkk.btl1.api.APIService
import com.kienkk.btl1.model.APIResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.util.ArrayList
import android.content.IntentFilter

import android.R
import android.annotation.SuppressLint

import android.content.BroadcastReceiver
import android.database.Cursor
import android.os.Build
import android.provider.MediaStore
import android.widget.PopupMenu
import androidx.core.content.ContextCompat.getExternalFilesDirs
import androidx.core.content.ContextCompat.registerReceiver
import com.kienkk.btl1.model.SongModel
import kotlin.math.E


class HomePresenter (var homeInterface: HomeInterface, var context: Context){

    var data : APIResponse? = null
    var uriImageAll = mutableListOf<String>()
    var listCata = mutableListOf<String>()


    fun getDataFromAPI() {
        APIService.create().getData.enqueue(object : Callback<APIResponse?> {
            override fun onResponse(call: Call<APIResponse?>, response: Response<APIResponse?>) {
                data = response.body()!!
                addUriToList()
                homeInterface.StartFragment(data!!,uriImageAll)
                addFolder()
            }

            override fun onFailure(call: Call<APIResponse?>, t: Throwable) {

            }

        })
    }

    fun downloadFile(link: String,ivImage:ImageView,isDown:ImageView,nameImage: TextView){
        var downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        var request = DownloadManager.Request(Uri.parse(link))
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setDestinationInExternalFilesDir(context,Environment.DIRECTORY_PICTURES,link.substringAfterLast("/"))
        var enqueue = downloadManager.enqueue(request)
        broadcast(enqueue,downloadManager,ivImage, isDown,nameImage,context)
    }

    fun broadcast(enqueue: Long,dm:DownloadManager,ivImage:ImageView,isDown:ImageView,nameImage: TextView,context: Context){
        val receiver: BroadcastReceiver = object : BroadcastReceiver() {
            @SuppressLint("Range")
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent
                val downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0)
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE == action.action) {
                    val query = DownloadManager.Query()
                    query.setFilterById(enqueue)
                    val c: Cursor = dm.query(query)
                    if (c.moveToLast()) {
                        val columnIndex: Int = c.getColumnIndex(DownloadManager.COLUMN_STATUS)
                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex) && downloadId == c.getLong(0)) {
                            // set Image
                            val uriString: String = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))
                            val filename = uriString.substringAfterLast('/')
                            val sd = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                            val dest = File(sd, filename)
                            var bitmap = BitmapFactory.decodeFile(dest.absolutePath)
                            ivImage.setImageBitmap(bitmap)
                            nameImage.text = uriString.substringAfterLast("/")
                            isDown.visibility = View.INVISIBLE
                        }

                    }
                }
            }
        }
        context.registerReceiver(receiver, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
    }


    fun addUriToList(){
        if (data != null){
            for (i in data!!.listPhotoFrames){
                for ( j in i.defines[0].start .. i.defines[0].end){
                    uriImageAll.add(data!!.start_link + i.folder + "/" + i.folder + "_frame_" + j +".png")
                }
            }
        }
    }

    fun addFolder(){
        if (data != null){
            for (i in data!!.listPhotoFrames){
                listCata.add(i.name)
            }
        }
    }
}