package com.kienkk.btl1.model

data class SongModel(
    var path: String,
    var name: String,
    var album: String,
    var artist: String
)
