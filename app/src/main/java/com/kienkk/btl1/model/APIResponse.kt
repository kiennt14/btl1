package com.kienkk.btl1.model

data class APIResponse(
    var start_link: String,
    var listPhotoFrames: MutableList<ListPhoto>
)
data class ListPhoto(
    var name: String,
    var name_vi: String,
    var folder: String,
    var icon: String,
    var cover: String,
    var totalImage: Int,
    var lock: Boolean,
    var openPackageName: String,
    var defines: MutableList<Defines>
)
data class Defines(
    var start: Int,
    var end: Int,
    var totalCollageItemContainer: Int,
    var indexDefineCollage: Int
)
