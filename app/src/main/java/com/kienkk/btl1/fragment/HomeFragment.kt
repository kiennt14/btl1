package com.kienkk.btl1.fragment

import android.content.Context
import android.database.Cursor
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kienkk.btl1.MainActivity
import com.kienkk.btl1.R
import com.kienkk.btl1.adapter.CataAdapter
import com.kienkk.btl1.adapter.ImageAdapter
import com.kienkk.btl1.model.ListPhoto
import com.kienkk.btl1.model.SongModel
import java.io.File
import java.lang.Exception


class HomeFragment(var uriImage: MutableList<String>, var listCata: MutableList<ListPhoto>, var activity: MainActivity) : Fragment(){

    lateinit var mediaPlayer: MediaPlayer
    var i = 0
    var listSong = loadFileSong()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.fragment_home, container, false)

        var rclImage = view.findViewById<RecyclerView>(R.id.viewImage)
        var rclCata = view.findViewById<RecyclerView>(R.id.viewCata)
        var ivImage = view.findViewById<ImageView>(R.id.iv_image)
        var nameImage = view.findViewById<TextView>(R.id.name_file)
        var tvNameSong = view.findViewById<TextView>(R.id.name_song)
        var btnNext = view.findViewById<ImageView>(R.id.btn_next)
        var btnPlay = view.findViewById<ImageView>(R.id.btn_play)
        var btnPre = view.findViewById<ImageView>(R.id.btn_previous)
        var tvNameSinger = view.findViewById<TextView>(R.id.name_singer)
        var menu = view.findViewById<ImageView>(R.id.menu)
        var popup = PopupMenu(activity,menu)

        popup.menuInflater.inflate(R.menu.menu, popup.menu)
        popup.setOnMenuItemClickListener(object: PopupMenu.OnMenuItemClickListener{
            override fun onMenuItemClick(item: MenuItem?): Boolean {
                i = item!!.itemId
                playSong(tvNameSong,tvNameSinger)
                btnPlay.setImageResource(R.drawable.ic_play)
                return true
            }

        })


        rclCata.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        var adapterCata = CataAdapter(listCata,rclImage,rclCata)
        rclCata.adapter = adapterCata

        rclImage.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        var adapter = ImageAdapter(container!!.context, uriImage, listCata,rclImage, adapterCata,activity,ivImage,nameImage)
        rclImage.adapter = adapter


        if (listSong!!.size != 0){

            for (i in listSong){
                popup.menu.add(0,listSong.indexOf(i),0,i.name)
            }

            menu.setOnClickListener {
                popup.show()
            }


            tvNameSong.text = listSong?.get(i)!!.name
            tvNameSinger.setText(listSong[i].artist)
            try {
                mediaPlayer = MediaPlayer.create(context, Uri.parse(listSong?.get(i).path))
            }catch (e: Exception){
                Toast.makeText(activity,"Khong the khoi tao nhac", Toast.LENGTH_SHORT).show()
            }
            btnNext.setOnClickListener {
                if (i < listSong!!.size - 1){
                    i++
                    playSong(tvNameSong,tvNameSinger)
                    btnPlay.setImageResource(R.drawable.ic_play)
                }else{
                    i=0
                    playSong(tvNameSong,tvNameSinger)
                    btnPlay.setImageResource(R.drawable.ic_play)
                }
            }
            btnPre.setOnClickListener {
                if (i != 0){
                    i--
                    playSong(tvNameSong,tvNameSinger)
                    btnPlay.setImageResource(R.drawable.ic_play)
                }else{
                    i=listSong!!.size-1
                    playSong(tvNameSong,tvNameSinger)
                    btnPlay.setImageResource(R.drawable.ic_play)
                }
            }
            btnPlay.setOnClickListener {
                if (mediaPlayer.isPlaying){
                    mediaPlayer.pause()
                    btnPlay.setImageResource(R.drawable.ic_pause)
                }else{
                    btnPlay.setImageResource(R.drawable.ic_play)
                    var length = mediaPlayer.getCurrentPosition()
                    mediaPlayer.seekTo(length)
                    mediaPlayer.start()
                }
            }
        }

        return view
    }

    fun loadFileSong(): MutableList<SongModel>{
        var listSongModel = mutableListOf<SongModel>()
        var uri: Uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        var projection = arrayOf(MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.TITLE, MediaStore.Audio.AudioColumns.ALBUM, MediaStore.Audio.ArtistColumns.ARTIST)
        var c: Cursor? = activity.getContentResolver()!!.query(uri, projection,null,null,null)

        if (c != null) {
            while (c.moveToNext()) {

                var path = c.getString(0)
                var name = c.getString(1)
                var album = c.getString(2)
                var artist = c.getString(3)

                var audioModel = SongModel(path, name, album, artist)

                listSongModel.add(audioModel)
            }
            c.close()
        }

        return listSongModel
    }

    fun playSong(tvName: TextView, tvNameSinger: TextView){
        tvName.text = listSong?.get(i)!!.name
        tvNameSinger.setText(listSong[i].artist)
        if (mediaPlayer != null){
            mediaPlayer.reset()
        }
        try {
            mediaPlayer = MediaPlayer.create(context, Uri.parse(listSong.get(i).path))
            mediaPlayer.start()
        }catch (e: Exception){
            Toast.makeText(activity,"Khong the phat nhac", Toast.LENGTH_SHORT).show()
        }

    }

}